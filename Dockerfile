FROM openjdk:12
ADD target/docker-spring-boot-rafaelrosa.jar docker-spring-boot-rafaelrosa.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker-spring-boot-rafaelrosa.jar"]
